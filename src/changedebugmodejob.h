/*
  Copyright (c) 2019-2020 Laurent Montel <montel@kde.org>

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This library is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
  License for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; see the file COPYING.LIB.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.

*/

#ifndef CHANGEDEBUGMODEJOB_H
#define CHANGEDEBUGMODEJOB_H

#include "libkdebugsettings_export.h"
#include "loggingcategory.h"
#include <QString>
#include <QStringList>

class LIBKDEBUGSETTINGS_EXPORT ChangeDebugModeJob
{
public:
    ChangeDebugModeJob();
    ~ChangeDebugModeJob();

    Q_REQUIRED_RESULT bool start();

    void setDebugMode(const QString &mode);
    Q_REQUIRED_RESULT QString debugMode() const;

    Q_REQUIRED_RESULT QStringList loggingCategoriesName() const;
    void setLoggingCategoriesName(const QStringList &loggingCategoriesName);

    Q_REQUIRED_RESULT bool canStart() const;
    Q_REQUIRED_RESULT bool debugModeIsValid(const QString &value) const;
    Q_REQUIRED_RESULT LoggingCategory::LoggingType convertDebugModeToLoggingType(const QString &value);
    void setWithoutArguments(bool b);
    Q_REQUIRED_RESULT bool withoutArguments() const;

private:
    QString mDebugMode;
    QStringList mLoggingCategoriesName;
    bool mWithoutArguments = false;
};

#endif // CHANGEDEBUGMODEJOB_H
